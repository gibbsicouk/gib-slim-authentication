<?php

namespace App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $errors;

    /**
     * Check and validate each rule in a loop
     *
     * @param $request
     * @param array $rules
     * @return $this
     */
    public function validate($request, array $rules)
    {
        foreach ($rules as $field => $rule) {
            try{
                $rule->setName($field)->assert($request->getParam($field));
            } catch (NestedValidationException $e) {
                $this->errors[$field] = $e->getMessages();
            }
        }

        // Set errors as part of a session to pass to the middleware
        $_SESSION['errors'] = $this->errors;

        return $this;

    }

    /**
     * Check if validation failed
     *
     * @return bool
     */
    public function failed()
    {
        return !empty($this->errors);
    }


}