<?php

namespace App\Controllers\Auth;

use App\Models\User;
use App\Controllers\Controller;
use Respect\Validation\Validator as v;

/**
 * Class AuthController
 * @package App\Controllers\Auth
 */
class AuthController extends Controller
{
    /**
     *
     * Display the sign up form
     *
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getSignUp($request, $response)
    {

        return $this->view->render($response, 'auth/signup.twig');
    }

    /**
     *
     * Get user credentials from form and sign them up
     *
     * @param $request
     * @param $response
     * @return mixed
     */
    public function postSignUp($request, $response)
    {

        $validation = $this->validator->validate($request, [
            'email' => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'name' => v::notEmpty()->alpha(),
            'password' => v::noWhitespace()->notEmpty(),
        ]);

        if ($validation->failed()) {
            $this->flash->addMessage('danger', 'There was an error whilst attempting to sign you up.');
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }

        $user = User::create([
            'email' => $request->getParam('email'),
            'name' => $request->getParam('name'),
            'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
        ]);

        $this->flash->addMessage('info', 'You have been signed up for an account.');

        return $response->withRedirect($this->router->pathFor('home'));
    }

    /**
     *
     * Display the sign in form
     *
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getSignIn($request, $response)
    {
        return $this->view->render($response, 'auth/signin.twig');
    }

    public function postSignIn($request, $response)
    {
        $auth = $this->auth->attempt(
            $request->getParam('email'),
            $request->getParam('password')
        );

        if (!$auth) {
            $this->flash->addMessage('danger', 'Could not sign you in with those details.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->router->pathFor('home'));
    }

    public function getSignOut($request, $response)
    {
        $this->auth->logout();
        $this->flash->addMessage('info', 'You have been been signed out.');
        return $response->withRedirect($this->router->pathFor('home'));
    }


}